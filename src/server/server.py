import sys
import os
ROOT_FOLDER = os.path.dirname(os.path.abspath(os.pardir))
sys.path.insert(0, ROOT_FOLDER)
print(ROOT_FOLDER)

print(sys.path)

from flask import Flask
from flask import request
from flask_cors import CORS, cross_origin
import src.interface.game as GameManager
import json

app = Flask(__name__)
CORS(app)

@app.route("/")
def index():
    return "This is the Cage AI Game"

@app.route('/setup_new_game', methods=['POST'])
def setup_new_game():
    print("Debug: setup_new_game")
    data_input = request.json
    data_fields = ['num_players', 'num_bots', 'board_size_x', 'board_size_y', 'game_name']
    data_setup = {'num_players':1, 'num_bots':0, 'board_size_x':3, 'board_size_y':3, 'game_name':'default_game'}
    if data_input:
        for field in data_fields:
            if field in data_input:
                data_setup[field] = data_input[field]

    game_manager = GameManager.Game()
    game_id = game_manager.setup_game(**data_setup)
    return (json.dumps({"Status": 'OK', "game_id": game_id}),{'content-type':'application/json'})

@app.route('/fetch_game_players', methods=['POST'])
def fetch_game_players():
    print("Debug: fetch_game_players")
    data_input = request.json
    game_id = data_input['game_id']
    game_manager = GameManager.Game()
    players_ids = game_manager.fetch_game_players(game_id)
    return (json.dumps({"Status": 'OK', "players_ids": players_ids}),{'content-type':'application/json'})

@app.route('/get_saved_games', methods=['GET', 'POST'])
def get_saved_games():
    print("Debug: get_saved_games")
    game_manager = GameManager.Game()
    games = game_manager.get_saved_games()
    for game in games:
        game['_id'] = str(game['_id'])
    return (json.dumps({"Status": 'OK', "games": games}), {'content-type': 'application/json'})

@app.route('/fetch_game', methods=['POST'])
def fetch_game():
    print("Debug: fetch_game")
    data_input = request.json
    game_id = data_input['game_id']
    game_manager = GameManager.Game()
    game = game_manager.fetch_game(game_id)
    game['_id'] = str(game['_id'])
    return (json.dumps({"Status": 'OK', "game": game}), {'content-type': 'application/json'})

@app.route('/store_brain', methods=['POST'])
def store_brain():
    print("Debug: store_brain")
    data_input = request.json
    game_id = data_input['game_id']
    player_id = data_input['player_id']
    brain = data_input['brain']
    game_manager = GameManager.Game()
    result = game_manager.store_brain(game_id, player_id, brain)
    return (json.dumps({"Status": 'OK' if result is not False else "Failed"}), {'content-type': 'application/json'})

@app.route('/run_game', methods=['POST'])
def run_game():
    print("Debug: run_game")
    data_input = request.json
    game_id = data_input['game_id']
    game_manager = GameManager.Game()
    result = game_manager.run_game(game_id)
    return (json.dumps({"Status": 'OK', "result": result}), {'content-type': 'application/json'})

@app.route('/get_game_simulaiton', methods=['POST'])
def get_game_simulaiton():
    print("Debug: run_game")
    data_input = request.json
    game_id = data_input['game_id']
    game_manager = GameManager.Game()
    result = game_manager.get_game_simulaiton(game_id)
    return (json.dumps({"Status": 'OK', "result": result}), {'content-type': 'application/json'})


if __name__ == "__main__":
    app.run(debug=True,
            host='0.0.0.0',
            port='5000')


# How to run
# set FLASK_APP=server.py
# set FLASK_ENV=development
# flask run - -host = 0.0.0.0
