import pymongo
from bson.objectid import ObjectId # ObjectId(post_id)
from pymongo import ReturnDocument

class MongoAgent(object):
    def __init__(self):
        self.atlas_connection_string =  "mongodb+srv://db-user:db-user@cluster0-t1j8u.gcp.mongodb.net/test?retryWrites=true"
        self.client = pymongo.MongoClient(self.atlas_connection_string)
        self.db = self.client['cage_ai']

    def create_new_game(self, game):
        result = self.db['games'].insert_one(game)
        return result.inserted_id

    def get_game(self, game_id):
        query = {'_id': ObjectId(game_id)}
        result = self.db['games'].find_one(query)
        return result

    def update_game(self, game):
        query = {'_id': game['_id']}
        result = self.db['games'].replace_one(query, game)
        return result

    def get_all_games(self):
        query = {}
        projection = {'_id':1, 'game_name':1}
        result = self.db['games'].find(query, projection)
        return list(result)

    def save_simulation(self, game_id, simulation):
        query = {'_id': game_id}
        query_data = {'_id': game_id, 'data': simulation}
        result = self.db['simulations'].replace_one(query, query_data,upsert=True)
        return result

    def get_game_simulation_id(self, game_id):
        query = {'_id': game_id}
        result = self.db['simulations'].find_one(query)
        return result


if __name__ == '__main__':
    obj = MongoAgent()
    print(obj)