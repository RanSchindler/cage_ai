import random
import dill
from src.bot.actions import NoAction, Attack, Move, AttackMove, MoveAttack

def default_brain(api):
    return NoAction()

def random_brain(api):
    return [NoAction(), Attack(), Move(direction='UP'), AttackMove(direction='UP'), MoveAttack(direction='UP')][random.randint(0,4)]

def another_random_brain(api):
    actions_options = [
        NoAction(),
        Move(direction='UP',    step=1),
        Move(direction='UP',    step=2),
        Move(direction='UP',    step=3),
        Move(direction='DOWN',  step=1),
        Move(direction='DOWN',  step=2),
        Move(direction='DOWN',  step=3),
        Move(direction='LEFT',  step=1),
        Move(direction='LEFT',  step=2),
        Move(direction='LEFT',  step=3),
        Move(direction='RIGHT', step=1),
        Move(direction='RIGHT', step=2),
        Move(direction='RIGHT', step=3),
        Attack(player=api.get_strongest_player()),
        AttackMove(direction='UP',    player=api.get_strongest_player()),
        AttackMove(direction='DOWN',  player=api.get_weakest_player()),
        AttackMove(direction='LEFT',  player=api.get_strongest_player()),
        AttackMove(direction='RIGHT', player=api.get_weakest_player()),
        MoveAttack(direction='UP',    player=api.get_strongest_player()),
        MoveAttack(direction='DOWN',  player=api.get_weakest_player()),
        MoveAttack(direction='LEFT',  player=api.get_strongest_player()),
        MoveAttack(direction='RIGHT', player=api.get_weakest_player()),
    ]
    return actions_options[random.randint(0,len(actions_options)-1)]


def advanced_brain(api):
    all_players = api.get_players()
    print("All players",all_players)
    strongest = api.get_strongest_player()
    print("Strongest:",strongest)
    weakest = api.get_weakest_player()
    print("weakest:", weakest)
    strong_location = api.get_player_location(strongest)
    print("strong_location:", strong_location)
    weak_location = api.get_player_location(weakest)
    print("weak_location:", weak_location)
    strong_life = api.get_player_life(strongest)
    print("strong_life:", strong_life)
    weak_life = api.get_player_life(weakest)
    print("weak_life:", weak_life)
    my_location = api.get_my_location()
    print("My location",my_location)
    my_life = api.get_my_life()
    print("My life",my_life)

    return [NoAction(), Attack(), Move(direction='UP'), AttackMove(direction='UP'), MoveAttack(direction='UP')][
        random.randint(0, 4)]