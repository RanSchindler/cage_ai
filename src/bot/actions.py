class BaseAction(object):
    def __init__(self):
        self.step = 0
        self.action = None


class NoAction(BaseAction):
    def __init__(self):
        super().__init__()


class Move(BaseAction):
    def __init__(self, direction=None, step=1):
        super().__init__()
        self.step = min(3, max(0, step))
        self.action = ['Move']
        self.direction = direction
        self.attack_power = 0


class Attack(BaseAction):
    def __init__(self, player=None):
        super().__init__()
        self.step = 0
        self.action = ['Attack']
        self.player = player
        self.attack_power = 1


class AttackMove(BaseAction):
    def __init__(self,  direction=None, player=None):
        super().__init__()
        self.step = 2
        self.action = ['Attack', 'Move']
        self.direction = direction
        self.player = player
        self.attack_power = 0.6


class MoveAttack(BaseAction):
    def __init__(self,  direction=None, player=None):
        super().__init__()
        self.step = 2
        self.action = ['Move', 'Attack']
        self.direction = direction
        self.player = player
        self.attack_power = 0.8