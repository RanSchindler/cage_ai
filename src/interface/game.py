import src.db.agent as db_agent
import uuid
import pickle
import dill
import random
import src.bot.brain as brains
import src.board.board as board
import math
import json
from src.bot.actions import NoAction, Attack, Move, AttackMove, MoveAttack

class PlayerData(object):
    ID = 0

    def __init__(self, player_id, life):
        self.brain = str(dill.dumps(brains.another_random_brain))
        self.player_id = player_id
        self.life = life

    def get_json(self):
        result = {
            'brain': self.brain,
            'player_id': self.player_id,
            'life': self.life,
            'ID': PlayerData.ID,
            'status': 'alive'
        }
        PlayerData.ID += 1

        return result


class Game(object):

    def __init__(self):
        self.db_agent = db_agent.MongoAgent()

    def init_game(self, game):
        num_players = game['num_players']
        x = game['board_size_x']
        y = game['board_size_y']

        players_locations = set()
        while len(players_locations) < num_players:
            players_locations.add((random.randint(0,x-1),random.randint(0,y-1)))

        for location,player in zip(players_locations, list(game['players'].values())):
            game['current_board'][location[1]][location[0]] = player['ID']
            game['players'][player['player_id']]['location'] = location

        return game

    def setup_game(self, num_players=0, num_bots=0, board_size_x=3, board_size_y=3, game_name='default_game'):
        game = dict()
        game['num_players'] = num_players
        game['players'] = {player_id:PlayerData(player_id,(board_size_x+board_size_y)*1000).get_json() for player_id in [str(uuid.uuid4()) for num in range(num_players)]}
        game['num_bots'] = num_bots
        game['board_size_x'] = board_size_x
        game['board_size_y'] = board_size_y
        game['game_name'] = game_name
        game['last_attack'] = {}
        game['current_board'] = [[None] * board_size_x for _ in range(board_size_y)]
        game = self.init_game(game)
        game_id = self.db_agent.create_new_game(game)
        return {'game_id': str(game_id)}

    def fetch_game_players(self, game_id):
        game = self.fetch_game(game_id)
        result = list(game['players'].values())
        for player in result:
            del player['brain']
        return result

    def store_brain(self, game_id, player_id, brain):
        '''
        funciton = dill.loads(eval(str(dill.dumps(str))))
        '''

        #s_brain = dill.dumps(brain)
        s_brain = brain
        game = self.fetch_game(game_id)

        try:
            user_function = dill.loads(eval(s_brain))
            if not callable(user_function):
                return False
        except:
            return False

        if player_id in game['players']:
            game['players'][player_id]['brain'] = s_brain
            result = self.db_agent.update_game(game)
            return result
        return False

    def get_saved_games(self):
        result = self.db_agent.get_all_games()
        return result

    def fetch_game(self, game_id):
        game = self.db_agent.get_game(game_id)
        return game

    def run_game(self, game_id):
        game = self.run_simulation(game_id)
        all_players = sorted(game['players'].values(), key=lambda x: x['life'], reverse=True)
        result = [{'ID':player['ID'],'Life':player['life']} for player in all_players]
        return result

    def run_simulation(self, game_id):
        simulation = []
        game = self.db_agent.get_game(game_id)
        counter = 0
        while True:
            counter +=1
            players = game['players']
            all_players = [player for player in players.values() if player['status'] == 'alive']
            random.shuffle(all_players)
            game['last_attack'] = {}
            for player in all_players:
                api = board.Api(game, player)
                print("Player", player['ID'])
                brain = dill.loads(eval(player['brain']))

                #action = brain(api)
                #game = self.handle_action(game, player, action)
                try:
                    action = brain(api)
                    game = self.handle_action(game, player, action)
                    pass
                except:
                    continue

            all_life = [player['life'] for player in game['players'].values()]
            all_alive = [life for life in all_life if life>0]
            print('---------------------------')
            simulation.append(json.dumps(self.generate_simulation_step(game)))
            if len(all_alive) <= 1:
                break
            print('---------------------------',counter)
            for player in game['players'].values():
                print(player['ID'],' ',player['life'])

        self.db_agent.save_simulation(game_id, simulation)

        return game

    def generate_simulation_step(self, game):
        simulation_step = {}
        simulation_step['locations'] = {}
        current_board = game['current_board']
        for y, row in enumerate(current_board):
            for x, item in enumerate(row):
                if item is not None:
                    simulation_step['locations'][item] = [y,x]
        simulation_step['last_attack'] = game['last_attack']
        simulation_step['life'] = {}
        for p in game['players'].values():
            simulation_step['life'][p['ID']] = p['life']
        return simulation_step

    def get_game_simulaiton(self, game_id):
        game = self.fetch_game(game_id)
        result = {}
        result['simulation'] = self.db_agent.get_game_simulation_id(game_id)
        result['x'] = game['board_size_x']
        result['y'] = game['board_size_y']
        return result

    def handle_action(self, game, player, action):
        if action.action is None:
            print('No Action')
        else:
            for act in action.action:
                if act == 'Move':
                    print('Move', action.direction)
                    if action.direction is not None:
                        game = self.hanlde_move_to_direction(game, player, action)
                elif act == 'Attack':
                    game = self.handle_attack(game, player, action)
                    pass

        return game

    def hanlde_move_to_direction(self, game, player, action):
        direction = action.direction
        ID = player['ID']
        player_location = player['location']
        if direction is 'UP':
            new_location = (player_location[0], max(player_location[1]-1,0))
        elif direction is 'DOWN':
            new_location = (player_location[0], min(player_location[1] + 1, game['board_size_y']-1))
        elif direction is 'LEFT':
            new_location = (max(player_location[0]-1, 0), player_location[1])
        elif direction is 'RIGHT':
            new_location = (min(player_location[0]+1,  game['board_size_x']-1), player_location[1])

        if game['current_board'][new_location[1]][new_location[0]] is None:
            game['current_board'][new_location[1]][new_location[0]] = game['current_board'][player_location[1]][player_location[0]]
            game['current_board'][player_location[1]][player_location[0]] = None
            game['players'][player['player_id']]['location'] = new_location

        return game

    def handle_attack(self, game, player, action):
        if action.player is None:
            relevant_players = [player for player in game['players'].values() if player['life']>0]
            action.player = relevant_players[random.randint(0,len(relevant_players)-1)]
        if player['ID'] != action.player['ID']:
            player_location = player['location']
            enemy_location = action.player['location']
            distance = abs(player_location[0] - enemy_location[0]) + abs(player_location[1] - enemy_location[1])
            demage = math.exp(2 - (0.5 * distance)) * 100 * action.attack_power # e^{2-0.5x}
            game['players'][action.player['player_id']]['life'] -= demage
            print('Attack ', player['ID'], ' ', action.player['ID'],' ',demage)
            game['last_attack'][player['ID']] = action.player['ID']

        if game['players'][action.player['player_id']]['life'] <= 0:
            game['players'][action.player['player_id']]['status'] = 'dead'
            action_player_location = action.player['location']
            print("DEAD")
            #game['current_board'][action_player_location[1]][action_player_location[0]] = "X"+str(action.player['ID'])
        return game


if __name__ == '__main__':
    obj = Game()
    # print("Creating new game")
    new_game = obj.setup_game(num_players=5,board_size_x=5, board_size_y=5)
    print("Game ID: ",new_game)
    print("Get Game By ID")
    game = obj.fetch_game(new_game['game_id'])
    print("Game: ",game)
    players = obj.fetch_game_players(new_game['game_id'])
    print("Players: ",players)

    # def f(x):
    #     return x
    # update_brain = obj.store_brain(new_game['game_id'], players[0], f)
    # print("Update Brain:", update_brain.raw_result)
    # game = obj.fetch_game(new_game['game_id'])
    # print("Game: ", game)
    # print("P1 engine: ", dill.loads(game['players'][players[0]]['brain'])("TEST"))

    # all_games = obj.get_saved_games()
    # for g in all_games:
    #     print("Games: ", g)

    result = obj.run_game(new_game['game_id'])
    print("Game Result: ", result)