class Api(object):
    def __init__(self, game, player):
        self.game = game
        self.player = player
        self.all_players = [player for player in self.game['players'].values() if player['status'] == 'alive' and player['ID']!=self.player['ID'] ]

    def get_players(self):
        return self.all_players

    def get_strongest_player(self):
        result = sorted(self.all_players, key=lambda x: x['life'], reverse=True)
        return result[0]

    def get_weakest_player(self):
        result = sorted(self.all_players, key=lambda x: x['life'], reverse=False)
        return result[0]

    def get_player_location(self, player):
        return player['location']

    def get_player_life(self, player):
        return player['life']

    def get_my_location(self):
        return self.player['location']

    def get_my_life(self):
        return self.player['life']

